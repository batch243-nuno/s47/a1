// console.log(`Hello World`);

// Document Object Model (DOM)

const firstNameInput = document.querySelector('#firstName');
const lastNameInput = document.querySelector('#lastName');
// console.log(firstNameInput);
const input = document.querySelectorAll('.nameInput');
// console.log(input);
const element = document.querySelectorAll('span');
// console.log(element);
const attr = document.querySelectorAll('input[type]');
// console.log(attr);
const fullName = document.querySelector('#fullName');

// event listeners : change, click, load, keydown, keyup
const colorPicker = document.querySelector('#colorFullName');

const fullNameFunction = () => {
  fullName.setAttribute('style', `color: ${colorPicker.value}`);
  fullName.innerHTML = `${firstNameInput.value} ${lastNameInput.value}`;
};

firstNameInput.addEventListener('keyup', fullNameFunction);
lastNameInput.addEventListener('keyup', fullNameFunction);
colorPicker.addEventListener('click', fullNameFunction);
